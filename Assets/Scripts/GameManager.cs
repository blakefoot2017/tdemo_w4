﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{

    public float tickTime = 1.0f;
    public float lastTick = 0.0f;

    public PlayerController playerController;
    public InventoryManager inventoryManager;

    // Update is called once per frame
    void Update()
    {
        if (Time.time > (lastTick + tickTime))
        {
            lastTick = lastTick + tickTime;
            playerController.ProcessMorale();
            if (playerController.inCorruptionZone) { playerController.ModifyCorruption(40); }
            if (playerController.berserkerTimeLeft != 0) { playerController.berserkerTimeLeft--; }

            foreach (EquipmentObject i in inventoryManager.playerInventory)
            {
                if (i != null)
                {
                    if (i.isPassive)
                    {
                        playerController.ModifyHealth(i.passiveHealth);
                        playerController.ModifyMorale(i.passiveMorale);
                        playerController.ModifyCorruption(i.passiveCorruption);
                    }
                }
            }
        }

        

    }
}
