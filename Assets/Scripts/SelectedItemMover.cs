﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SelectedItemMover : MonoBehaviour
{
    Vector3 newVector;

    public void select1stHotkey()
    {
        newVector = new Vector3(-96.0f, 1.0f, 0.0f);
        GetComponent<RectTransform>().localPosition = newVector;
    }
    public void select2ndHotkey()
    {
        newVector = new Vector3(-32.0f, 1.0f, 0.0f);
        GetComponent<RectTransform>().localPosition = newVector;
    }
    public void select3rdHotkey()
    {
        newVector = new Vector3(32.0f, 1.0f, 0.0f);
        GetComponent<RectTransform>().localPosition = newVector;
    }
    public void select4thHotkey()
    {
        newVector = new Vector3(96.0f, 1.0f, 0.0f);
        GetComponent<RectTransform>().localPosition = newVector;
    }
}
