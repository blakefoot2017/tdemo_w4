﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Debug = UnityEngine.Debug;

public class EnemyDamege : MonoBehaviour
{
    

    public int health;
    // Start is called before the first frame update
    void Update()
    {
        if (health <= 0)
        {
            Destroy(gameObject);

        }
    }


    public void TakeDamege(int damage)
    {
        health -= damage;
        UnityEngine.Debug.Log("damage taken");
    }
}
