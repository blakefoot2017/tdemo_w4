﻿﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMovement : MonoBehaviour
{
    public float enemyMoveSpeed;
    public float enemyJumpHeight;
    public Rigidbody2D enemyRigidbody2D;
    public LayerMask playerMask;
    public float aggroRange;
    private bool isJumping;
    public Transform target;

    public Transform ENattackPos;
    public float attackRange;
    public float attackStart;
    private float betweenAttack;
    public LayerMask enemyDetector;
    public int health;
    public int enDamage;
    private void Awake()
    {
        enemyRigidbody2D = GetComponent<Rigidbody2D>();
    }


    void Update()
      {
         MoveEnemy();
         if (betweenAttack <= 0)
        {
    
                Collider2D[] enemiesToHit = Physics2D.OverlapCircleAll(ENattackPos.position, attackRange, enemyDetector);
                for (int i = 0; i < enemiesToHit.Length; i++)
                {
                  //enemiesToHit[i].GetComponent<KriegMovement>().playerController.ModifyHealth(-enDamage);
                }

       betweenAttack = attackStart;
          }
           else
           {
               betweenAttack -= Time.deltaTime;
           }
          if (health <= 0)
          {
              Destroy(gameObject);

          }
      }

    void MoveEnemy()
    {
        RaycastHit2D hit = Physics2D.Raycast(transform.position, (target.position - transform.position).normalized, aggroRange, playerMask);
        if (hit.collider != null && target.gameObject.GetComponent<KriegMovement>())//.alive)
        {
            if (target.position.x + 0.1f < transform.position.x)
            {
                Vector3 movement = new Vector3(-1f, 0f, 0f);
                transform.position += movement * Time.deltaTime * enemyMoveSpeed;
            }
            if (target.position.x - 0.1f > transform.position.x)
            {
                Vector3 movement = new Vector3(1f, 0f, 0f);
                transform.position += movement * Time.deltaTime * enemyMoveSpeed;
            }

            if (target.position.y > transform.position.y + 1f)
            {
                if (!isJumping)
                {
                    enemyJump();
                }
            }
        }
    }

    void enemyJump()
    {
        isJumping = true;
        enemyRigidbody2D.AddForce(Vector2.up * enemyJumpHeight);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.layer == 9)
        {
            isJumping = false;
        }
    }
    public void TakeDamege(int damage)
    {
        health -= damage;
        UnityEngine.Debug.Log("damage taken");
    }

    void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(ENattackPos.position, attackRange);

    }
}
